FROM alpine
RUN apk add --no-cache nodejs npm

RUN adduser bb-webhook-responder --disabled-password --gecos -s /sbin/nologin
USER bb-webhook-responder
WORKDIR /home/bb-webhook-responder

COPY package* ./
RUN npm set progress=false && \
    npm config set depth 0 && \
    npm install
COPY ./src/main ./src/main
COPY package* ./
RUN npm install

EXPOSE 3000
CMD [ "node", "src/main/index.js" ]