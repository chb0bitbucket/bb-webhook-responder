Takes BB Webhooks and responds automatically.
Currently using a lambda but because of the real need to cache calls to BB API
it might need to be moved to a container so that a trivial cache can be used.