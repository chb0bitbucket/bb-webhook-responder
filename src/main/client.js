'use strict'
const axios = require('axios').default;
const {cache, hours, minutes} = require('./cache');

const {BB_WORKSPACE, BB_USERNAME, BB_PASSWORD} = process.env;


if (!BB_WORKSPACE || !BB_USERNAME || !BB_PASSWORD) {
    console.log(`Not set: WORKSPACE, USERNAME, PASSWORD ${BB_WORKSPACE}, ${BB_USERNAME}, ${BB_PASSWORD ? '****' : ''}`);
    process.exit(-1)
}


const httpConfig = {
    headers: {
        'content-type': 'application/json',
        accept: 'application/json'
    },
    auth: {
        username: BB_USERNAME,
        password: BB_PASSWORD
    }
};

const get = (link) => axios.get(link, httpConfig).then(results => results.data);
const put = (link, payload) => axios.put(link, payload, httpConfig).then(results => results.data);

const app = require('../../package');
const repo = app.repository.url.match('.*/(.*)\.git')[1];
const test = () => Promise.all([
    get(`https://api.bitbucket.org/1.0/groups/${BB_WORKSPACE}/`),
    get(`https://api.bitbucket.org/2.0/repositories/${BB_WORKSPACE}/?page=1&pagelen=0`)
]).catch(e => {
    console.error('%s -> %s',e.config.url,JSON.stringify(e.response.data,null,2));
    console.error(`WORKSPACE: ${BB_WORKSPACE}, USERNAME: ${BB_USERNAME}, PASSWORD ${BB_PASSWORD ? '****' : BB_PASSWORD}`)
    process.exit(-1)
});

const updatePr = (prlink, title, users) => {
    return put(prlink, {
        title: title,
        reviewers: users.map(u => {
            return {uuid: u.uuid}
        })
    }).then(result => {
        return {
            reviewers: result.reviewers.map(r => {
                return {
                    display_name: r.display_name,
                    uuid: r.uuid
                }
            })
        }
    }).finally(() => console.log(prlink))
};

const getUser = cache (
    (user) => get(`https://api.bitbucket.org/2.0/users/${encodeURIComponent(user)}/?fields=account_status,display_name,uuid`).catch(e => ({ account_status: 'unknown' })),
    hours(8)
);

const getGroupMembers = cache(
    (group) => {
        return get(`https://api.bitbucket.org/1.0/groups/${BB_WORKSPACE}/${group.toLowerCase()}/members`)
            .then(users => users.map(u => u.uuid))
            .catch(e => {
                console.warn("unable to get members for group %s: %d %s", group, e.response.data);
                return Promise.resolve([]);
            });
    },
    hours(8)
);

module.exports = {
    getUser,
    updatePr,
    getGroupMembers,
    get,
    put,
    test
};
