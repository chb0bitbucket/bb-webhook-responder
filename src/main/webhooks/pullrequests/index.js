'use strict';

const {getGroupMembers, getUser, updatePr, get, test} = require('../../client');
const fileLookup = require('./lookup')();
const {cache, minutes, hours} = require('../../cache');

const toReviewers = (details) => {
    return Promise.all(
            details.changes
                .map(fileLookup)
                .filter(e => e).map(getGroupMembers)
        )
        .then(groupMembers => ({
            ...details,
            newReviewers: groupMembers.flat().filter(e => e !== details.author)
        })
    );
};

const getDiffFiles = cache(
    (diffStat) => get(diffStat).then(diffs => diffs.values.map(diff => diff.old || diff.new).map(file => file.path)),
    hours(8)
);

const updateReviewers = (details) => {
    const {self, title, newReviewers, currentReviewers} = details;
    const potentialReviewers = newReviewers ? [...new Set(currentReviewers.concat(newReviewers))] : currentReviewers;
    return Promise.all(potentialReviewers.map(getUser))
        .then(users => users.filter(u => u.account_status === 'active')).then(users => updatePr(self, title, users));
};

const handler = cache(
    (event) => {
        const pr = event.pullrequest;
        return Promise.resolve({
            self: pr.links.self.href,
            title: pr.rendered.title.raw,
            author: pr.author.uuid,
            diffstat: pr.links.diffstat.href,
            reviewerLookup: fileLookup,
            currentReviewers: pr.reviewers.map(r => r.uuid)
        }).then(e => getDiffFiles(e.diffstat).then(changes => {
            return {
                changes: changes,
                ...e
            }
        }))
            .then(toReviewers)
            .then(updateReviewers)
    },
    minutes(1)
);

const schema = require('./schema');

module.exports = {
    schema,
    test,
    handler
};