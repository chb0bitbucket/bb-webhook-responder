'use strict';

const fileLookupJson = require('./fileLookup.json');

module.exports = (lookupTable) => {
    const table = lookupTable || fileLookupJson;
    return file => {
        const result = table.find(e => file.match(e.pattern));
        return result ? result.group : null
    }
};