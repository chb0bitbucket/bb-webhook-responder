'use strict';

const {validate} = require('jsonschema');
const express = require('express');
const {handler, schema, test} = require('./webhooks/pullrequests');

const app = express();
app.use(express.json());
app.use(express.text());
app.set('json spaces', 2);

const validatePayload = (req, res,next) => {
    const result = validate(req.body, schema);
    if(result.valid)
        return next();

    res.statusCode = 400;
    res.json(result.errors.map(e => {
        return {
            path: e.property,
            message: e.message
        }
    }));
};

app.post('/pullrequests', validatePayload, (req, res) => {
    handler(req.body).then(results => {
        res.status(200).json(results);
    }).catch(err => {
        console.error(`${JSON.stringify((err.response && err.response.data) || err.stack.split('\\n'))}`);
        res
            .status((err.response && err.response.status) || 500)
            .send('see the logs');
    })
});

app.get('/health',(req,res) => res.status(200).send('Happy as a clam'));

app.listen(3000, '0.0.0.0', () => {
    test().then(() => console.info("online"))
});