'use strict'

const cache = (f, evict) => {
    const c = {};

    setInterval((c) => {
        if (Object.keys(c).length > 1000) {
            const oldest = Object.entries(c).sort((f, s) => f[1].last - s[1].last).find(e => e);
            delete c[oldest[0]];
        }
    }, 10000, c);

    return (args) => {
        let entry = c.hasOwnProperty(args) && c[args];
        let copyOf = o => ({...o});

        if (!entry || evict( copyOf(args), copyOf(entry), copyOf(c)) ) {
            c[args] = {
                value: f(args),
                first: Date.now(),
                last: Date.now(),
                callCount: 1
            }
        } else {
            entry.last = Date.now();
            entry.callCount++;
        }
        return c[args].value;
    }
};

const hours = (num) => (key, meta) => Date.now() - meta.last > (60 * 60 * 1000 * num);
const minutes = (num) => (key, meta) => Date.now() - meta.last > (60 * 1000 * num);
const seconds = (num) => (key, meta) => Date.now() - meta.last > (1000 * num);
const count = (num) => (key, meta) => meta.callCount >= num;

module.exports = {
    cache,
    hours,
    minutes,
    seconds,
    count
}