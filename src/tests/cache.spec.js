var assert = require('assert');


describe('Given and empty cache', () => {
    describe('and an always false eviction policy', () => {
        const cache = require('../main/cache').cache
        it('should never evict the same key', (done) =>  {
            const input = `Christian ${Math.random()}`;
            let callCount = 0;
            const cached = cache((s) => {
                callCount++;
                return s;
            },() => false);
            let result = cached(input);
            assert.deepEqual(result,input);
            assert.equal(callCount,1);

            result = cached(input);
            assert.deepEqual(result,input);
            assert.equal(callCount,1);

            const me = cached('christian');
            assert.deepEqual(me,'christian');
            assert.equal(callCount,2);
            done()
        });

    });
    describe('and an always true eviction policy', () => {
        const cache = require('../main/cache').cache
        it('should never cache', (done) =>  {
            const input = `Christian ${Math.random()}`;
            let callCount = 0;
            const cached = cache((s) => {
                callCount++;
                return s;
            },() => true);
            let result = cached(input);
            assert.deepEqual(result,input);
            assert.equal(callCount,1);

            result = cached(input);
            assert.deepEqual(result,input);
            assert.equal(callCount,2);

            const me = cached('christian');
            assert.deepEqual(me,'christian');
            assert.equal(callCount,3);
            done()
        });

    });
    describe('and an eviction policy of no more than 5 calls', () => {
        const cache = require('../main/cache').cache
        it('it should evict the key on the 5th attempt', (done) =>  {
            const cached = cache((s) => `${s} ${Math.random()}`,(meta) => meta.callCount >= 5);

            const input = new Array(6).fill(`Christian`)
            const result = input.map(e => cached(e)).reduce((last,current) => last.add(current),new Set());

            assert.equal(result.size,2);

            done()
        });

    });
    describe('and the entry is more than 5 seconds old', () => {
        const cache = require('../main/cache').cache;
        it('should evict the key after 5 seconds', async () =>  {
            const input = `Christian`;
            const cached = cache(s => `${s} ${Math.random()}`,(meta) => Date.now() - meta.first > 500);

            const result = new Set();
            for(let i = 0; i < 6; i++){
                await new Promise(resolve => setTimeout(resolve, 200));
                result.add(cached(input))
            }
            assert.equal(result.size,2);
        });

    });

});
