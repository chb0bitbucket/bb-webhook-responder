var assert = require('assert');
const moxios = require('moxios');

/*
https://api.bitbucket.org/2.0/repositories/chb0bitbucket/chb0-deleteme/diffstat/chb0bitbucket/chb0-deleteme:7f087188ff7e%0D1ca4cadd6c4b?from_pullrequest_id=1
https://api.bitbucket.org/1.0/groups/chb0bitbucket/jenkinsfile/members
https://api.bitbucket.org/2.0/users/%7Bb4be70ff-304a-4c4a-bf90-5d5226fd28c5%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7Ba09a7b60-2545-41f7-b6bb-2dda835101da%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7Bdd46c0b3-7636-4b2f-afae-4fd9449a6c7d%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7Ba0d21c95-96a6-47e2-b97f-5f86b2b96867%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7Bd83df864-e1a6-4cc0-b71c-e683c3b43f5a%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7B9310618c-1a16-40de-a401-b69e6983f46e%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7B420cd0ac-b960-457c-9e19-9c7ad1b5025d%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7B8a9a1595-233f-4efe-911b-08fdb54279f6%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7B41475bd4-ef1b-4170-9959-e25e91a36119%7D/?fields=account_status,display_name,uuid
https://api.bitbucket.org/2.0/users/%7B75278745-3bee-4e91-b1f8-952c08b54554%7D/?fields=account_status,display_name,uuid
 */
describe('Accept pullrequest event', () => {
    const {handler} = require('../handlers');
    describe('pr created with no reviewers', () => {
        beforeEach(() => {
            moxios.install()
        });

        afterEach(function () {
            // import and pass your custom axios instance to this method
            moxios.uninstall()
        });
        it('should respond with all default reviewers  ', (done) =>  {
            const event = require('../samples/webhooks/pr-noreviewers');
            const expected = require('../handlers/defaultReviewers');
            const diffUrlMockedResponse = require('../samples/webhooks/with-reviewers-diff-link-response');

            moxios.stubRequest(
                'https://api.bitbucket.org/2.0/repositories/chb0bitbucket/chb0-deleteme/diffstat/chb0bitbucket/chb0-deleteme:7f087188ff7e%0D1ca4cadd6c4b?from_pullrequest_id=1',
                require('../../resources/api.bitbucket.org/2.0/repositories/chb0bitbucket/chb0-deleteme/diffstat/chb0bitbucket/diffstat')
            );
            const results = handler(event,{}, (err, res) => {
                assert.deepEqual(res, expected, "Response did not equal expected response.");
                done();
            });
        });
    });
    describe('pr created with some reviewers', () => {
        it('should respond with all default reviewers and original reviewers',  (done) =>  {
            const event = require('../samples/webhooks/pr-with-reviewers');
            const expected = require('../handlers/defaultReviewers');
            const results = lambda(event,{},done);
            results.should.deepEqual(expected)
        });
    });
    describe('pr created with some reviewers and author is also a default reviewer', () => {
        it('should respond with all default reviewers and original reviewers except the author ',  (done) =>  {
            const event = require('../samples/webhooks/pr-noreviewers');
            const expected = require('../handlers/defaultReviewers');
            const results = lambda(event,{},done);
            results.should.deepEqual(expected)
        });
    });
    describe('pr created with no registered files for review', () => {
        it('should respond with only the original reviewers',  (done) =>  {
            const event = require('./payloads/pr-with-reviewers');
            const expected = require('../../defaultReviewers');
            const results = lambda(event,{},done);
            results.should.deepEqual(expected)
        });
    });
});