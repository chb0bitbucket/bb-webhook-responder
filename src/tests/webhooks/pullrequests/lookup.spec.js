var assert = require('assert');
const moxios = require('moxios');


describe('Given a file to group lookup table', () => {


    describe('when we are looking for a simple file match', () => {
        it('should return a group', (done) => {
            const fileLookup = require('../../../main/webhooks/pullrequests/lookup')([{
                "pattern": "Jenkinsfile",
                "group": "someGroup"
            }]);
            const actual = fileLookup('Jenkinsfile');
            assert.equal(actual, 'someGroup');
            done();
        });
        describe('and we gave it no table', () => {
            it('should use the default table and return a group', (done) => {
                const fileLookup = require('../../../main/webhooks/pullrequests/lookup')();
                const actual = fileLookup('Jenkinsfile');
                assert.equal(actual, 'jenkinsfile');
                done();
            });
        });
    });
    describe('when we try to match a pattern', () => {
        it('should return a group', (done) => {
            const fileLookup = require('../../../main/webhooks/pullrequests/lookup')([{
                "pattern": ".*infra/.*\.(yml|yaml|cft)",
                "group": "someGroup"
            }]);
            const yml = fileLookup('infra/cfts/foo.yml');
            assert.equal(yml, 'someGroup');

            const yaml = fileLookup('infra/cfts/foo.yaml');
            assert.equal(yaml, 'someGroup');

            const cft = fileLookup('infra/cfts/foo.cft');
            assert.equal(cft, 'someGroup');

            done();
        });
    });
});